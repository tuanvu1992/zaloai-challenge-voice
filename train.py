import numpy as np
import os
from os.path import join, exists
from time import localtime, strftime
from keras.callbacks import Callback
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix

from load_data import *
from model import dilated_cnn


weight_list = [
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_10:56:54/weights_epoch115_0.8947.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_11:08:41/weights_epoch43_0.8545.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_11:21:09/weights_epoch60_0.9091.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_11:34:26/weights_epoch79_0.8364.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_11:48:31/weights_epoch32_0.8727.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_12:03:42/weights_epoch114_0.9091.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_12:19:50/weights_epoch35_0.8333.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_12:37:02/weights_epoch23_0.8654.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_12:59:31/weights_epoch50_0.8654.h5',
    '/home/messier/PycharmProjects/ZaloChallenge/checkpoints/2018-10-16_13:22:55/weights_epoch169_0.9000.h5'
]


class WeightsSaver(Callback):
    def __init__(self, model, save_name='model.h5', metrics='val_categorical_accuracy', criteria='max'):
        self.max_acc = 0.0
        self.min_loss = 1e3
        self.model = model
        self.metrics = metrics
        self.criteria = criteria
        st = strftime("%Y-%m-%d_%H:%M:%S", localtime())
        self.chkp_path = join('checkpoints', st)
        if not exists(self.chkp_path):
            os.makedirs(self.chkp_path)
        self.model.save(join(self.chkp_path, save_name))

    def on_epoch_end(self, epoch, logs=None):
        self.model.save_weights(join(self.chkp_path, 'latest_weight.h5'))
        if self.criteria == 'max':
            if self.max_acc < logs.get(self.metrics):
                self.max_acc = logs.get(self.metrics)
                print 'Best acc: %f' % self.max_acc
                self.model.save_weights(join(self.chkp_path, 'weights_epoch%d_%.4f.h5' % (epoch, self.max_acc)))
        if self.criteria == 'min':
            if self.min_loss > logs.get(self.metrics):
                self.min_loss = logs.get(self.metrics)
                print 'Best loss: %f' % self.min_loss
                self.model.save_weights(join(self.chkp_path, 'weights_epoch%d_%.4f.h5' % (epoch, self.min_loss)))


def train_accent():
    print 'Run training accent'
    x_train, y_train = load_train_data()
    y_train = y_train[:, 0:3]

    folds = list(StratifiedKFold(n_splits=10, shuffle=True, random_state=1).
                 split(x_train, np.argmax(y_train, axis=-1)))
    cf_matrix_all = []

    for j, (train_idx, val_idx) in enumerate(folds):
        print 'Training on folds %d' % j
        x_train_cv = x_train[train_idx]
        y_train_cv = y_train[train_idx]
        x_test_cv = x_train[val_idx]
        y_test_cv = y_train[val_idx]
        model = dilated_cnn(input_dim=84, output_dim=7, n_stage=4)
        model.compile(optimizer='adam',
                      loss='categorical_crossentropy',
                      metrics=['categorical_accuracy'])
        model.fit(x=x_train_cv, y=y_train_cv, validation_data=(x_test_cv, y_test_cv),
                  batch_size=16, epochs=200, shuffle=True, verbose=1,
                  callbacks=[WeightsSaver(model, metrics='val_categorical_accuracy',
                                          save_name='model_accent_fold_%d.h5' % j)])
        y_pred = model.predict(x=x_test_cv, batch_size=16)
        y_pred = np.argmax(y_pred, axis=1)
        y_test_cv = np.argmax(y_test_cv, axis=1)

        cf_matrix = np.array(confusion_matrix(y_test_cv, y_pred))
        np.save('data/confusion_matrix_%d.npy' % j, cf_matrix)
        cf_matrix_all.append(cf_matrix)
    cf_matrix_all = np.sum(cf_matrix_all, axis=0)
    print cf_matrix_all
    np.save('data/confusion_matrix_all.npy', cf_matrix_all)


def train_gender():
    print 'Run training gender'
    x_train, y_train = load_train_data()
    y_train = y_train[:, 3:]

    folds = list(StratifiedKFold(n_splits=10, shuffle=True, random_state=1).
                 split(x_train, np.argmax(y_train, axis=-1)))

    for j, (train_idx, val_idx) in enumerate(folds):
        print 'Training on folds %d' % j
        x_train_cv = x_train[train_idx]
        y_train_cv = y_train[train_idx]
        x_test_cv = x_train[val_idx]
        y_test_cv = y_train[val_idx]

        model = dilated_cnn(input_dim=84, output_dim=2, n_stage=4)
        model.compile(optimizer='adam',
                      loss='categorical_crossentropy',
                      metrics=['categorical_accuracy'])

        model.fit(x=x_train_cv, y=y_train_cv, validation_data=(x_test_cv, y_test_cv),
                  batch_size=64, epochs=100, shuffle=True, verbose=1,
                  callbacks=[WeightsSaver(model, metrics='val_categorical_accuracy',
                                          save_name='model_accent_fold_%d.h5' % j)])


def train_emo():
    print 'Run training emotion'

    x_train, y_train = load_emo_data()

    folds = list(StratifiedKFold(n_splits=10, shuffle=True, random_state=1).
                 split(x_train, np.argmax(y_train, axis=-1)))
    cf_matrix_all = []

    for j, (train_idx, val_idx) in enumerate(folds):
        print 'Training on folds %d' % j
        x_train_cv = x_train[train_idx]
        y_train_cv = y_train[train_idx]
        x_test_cv = x_train[val_idx]
        y_test_cv = y_train[val_idx]
        model = dilated_cnn(input_dim=84, output_dim=7, n_stage=4)
        model.compile(optimizer='adam',
                      loss='categorical_crossentropy',
                      metrics=['categorical_accuracy'])
        model.fit(x=x_train_cv, y=y_train_cv, validation_data=(x_test_cv, y_test_cv),
                  batch_size=16, epochs=200, shuffle=True, verbose=1,
                  callbacks=[WeightsSaver(model, metrics='val_categorical_accuracy',
                                          save_name='model_accent_fold_%d.h5' % j)])

        y_pred = model.predict(x=x_test_cv, batch_size=16)
        y_pred = np.argmax(y_pred, axis=1)
        y_test_cv = np.argmax(y_test_cv, axis=1)

        cf_matrix = np.array(confusion_matrix(y_test_cv, y_pred))
        np.save('emodata/confusion_matrix_%d.npy' % j, cf_matrix)
        cf_matrix_all.append(cf_matrix)
    cf_matrix_all = np.sum(cf_matrix_all, axis=0)
    print cf_matrix_all
    np.save('emodata/confusion_matrix_final.npy', cf_matrix_all)


if __name__ == '__main__':
    train_emo()
    # train_accent()
    # train_gender()


