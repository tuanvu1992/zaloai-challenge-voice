from keras.models import Model
from keras.layers import Dense, Conv1D, BatchNormalization, GlobalMaxPooling1D, \
    Input, Add, Multiply, Dropout, AveragePooling1D
import keras.optimizers as op
import keras.backend as K

from keras import regularizers


def DGR_block(input, n_filter, d_rate, kernel_size=3, l2_w=1e-3):
    i_n = BatchNormalization(momentum=0.8)(input)
    h = Conv1D(filters=n_filter, kernel_size=kernel_size, strides=1, dilation_rate=d_rate,
               padding='same', activation='tanh', kernel_regularizer=regularizers.l2(l2_w))(i_n)
    g = Conv1D(filters=n_filter, kernel_size=kernel_size, strides=1, dilation_rate=d_rate,
               padding='same', activation='sigmoid', kernel_regularizer=regularizers.l2(l2_w))(i_n)

    out = Multiply()([g, h])
    out = BatchNormalization(momentum=0.8)(out)
    out = Conv1D(filters=n_filter, kernel_size=1, strides=1, dilation_rate=1,
                 padding='same', activation='relu', kernel_regularizer=regularizers.l2(l2_w))(out)
    res = Add()([out, input])
    return out, res


def dilated_cnn(input_dim, output_dim, n_stage):
    l2_w = 1e-3
    x = Input(shape=(None, input_dim))
    h = Dense(256, activation='relu', kernel_regularizer=regularizers.l2(l2_w))(x)
    h = Dropout(0.5)(h)
    h = BatchNormalization(momentum=0.8)(h)
    h = Conv1D(128, kernel_size=7, strides=1, padding='same', activation='relu',
               kernel_regularizer=regularizers.l2(l2_w))(h)
    h = BatchNormalization(momentum=0.8)(h)
    h = Conv1D(128, kernel_size=3, strides=1, padding='same', activation='relu',
               kernel_regularizer=regularizers.l2(l2_w))(h)
    h = BatchNormalization(momentum=0.8)(h)
    h = AveragePooling1D(3, strides=2)(h)

    for i in range(n_stage):
        for d in [1, 2, 4, 8, 16, 32]:
        # for d in [1, 1, 1, 1, 1, 1]:
            o_i, h = DGR_block(h, n_filter=128, kernel_size=3, d_rate=d, l2_w=l2_w)
            if (i == 0) and (d == 1):
                out = o_i
            else:
                out = Add()([out, o_i])

        out = AveragePooling1D(3, 2)(out)
        h = AveragePooling1D(3, 2)(h)
    out = BatchNormalization(momentum=0.8)(out)
    out = Conv1D(filters=256, kernel_size=3, strides=1, padding='same', activation='relu',
                 kernel_regularizer=regularizers.l2(1e-3))(out)
    out = BatchNormalization(momentum=0.8)(out)
    out = Conv1D(filters=256, kernel_size=3, strides=1, padding='same', activation='relu',
                 kernel_regularizer=regularizers.l2(1e-3))(out)
    out = BatchNormalization(momentum=0.8)(out)
    out = AveragePooling1D()(out)
    out = Conv1D(filters=512, kernel_size=1, strides=1, padding='same', activation='relu',
                 kernel_regularizer=regularizers.l2(1e-3))(out)
    out = BatchNormalization(momentum=0.8)(out)
    out = GlobalMaxPooling1D()(out)
    out = Dense(1024, activation='relu', kernel_regularizer=regularizers.l2(1e-3))(out)
    out = Dropout(0.7)(out)
    out = Dense(output_dim, activation='softmax', kernel_regularizer=regularizers.l2(1e-3))(out)

    model = Model(x, out, name='dilated_cnn_model')

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
    return model
