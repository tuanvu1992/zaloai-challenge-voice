# Speech Emotion Classification 
Build by Ho Tuan Vu - Acoustic Information Science Lab  
Japan Advance Institute of Science and Technology  
Email: tuanvu.ho@jaist.ac.jp  

## Introduction
Source code for speech emotion recognition. This project is based on the top 2 project in ZaloAi Voice Accent & Gender Recognition Challenge 2018.  

### System Overview
The system contains 3 main parts: preprocessing, training and testing.  
![Alt text](imgs/system_overview.png)  

#### Preprocessing
-	Remove frame with energy < threshold  
-	Extracting features using window step = 5ms  
-	Segment long samples in to 3s length  
-	Wrap-padding short samples to 3s length  

### Acoustic features 
-	26 gammatone cepstral coefficients: Response from 128 gammatone filters -> cubic root -> 26-DCT transform  
-	Fundamental frequency F0 extracted using WORLD  
-	Root-mean-square energy RMSE  
-	Delta and delta-delta features  
The gammatone cepstral coefficients shows better performance than MFCC in overall.  

### Training
-	10-fold stratified-split training: ensure the same distribution between each fold  
-	Training 10 models in 200 epochs  
-	Optimizer = Adam  
-	Store the model with best accuracy of each fold  

### Network Architecture
The network architecture is based on the Wavenet architecture [1]. The network contains 4 consecutive dilated CNN blocks. Each block contain 5 dilated CNN cells with different dilation rate (1, 2, 4, 8, 16).  
![Alt text](imgs/network.png)  

### Benchmark
The un-norm and normalized confusion matrix using 10-fold cross-validation is shown below. The overall accuracy is around 0.874.
![Alt text](imgs/confusion_mat.png)   
![Alt text](imgs/confusion_mat_norm.png)

## Installation
```bash
git clone git@bitbucket.org:tuanvu1992/zaloai-challenge-voice.git
cd zaloai-challenge-voice
mkdir checkpoints
```

## Dependencies
-	python 2.7
-	Keras 2.2.0
-	Tensorflow-gpu
...
```bash
pip install -r requirements.txt
```

## Download emotion database
```bash
wget http://emodb.bilderbar.info/download/download.zip  
unzip download.zip -d emodata
```

## Running
Data pre-processing
```bash
python preprocessing.py
```

Run training
```bash
python train.py
```

Run test
```bash
python predict.py
```


