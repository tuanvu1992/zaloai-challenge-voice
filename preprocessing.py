import numpy as np
from tqdm import tqdm
from python_speech_features import delta, mfcc
import os
from os import listdir
from os.path import join, exists
from subprocess import call

from scipy.fftpack import dct
import pyworld
import gammatone.gtgram
import librosa

rnnoise_bin = '/app/rnnoise/examples/rnnoise_demo'


class DataProvider(object):
    def __init__(self, path, gfc_order=26, gt_channel=64,
                 frame_period=0.005):
        self.root_path = path
        self.wav_path = join(path, 'wav')
        self.wav_ns_path = join(path, 'wav_ns')
        self.cmp_ext = '.cmp'
        self.cmp_path = join(path, 'cmp')
        self.gfc_order = gfc_order
        self.gfc_dim = (gfc_order+2)*3
        self.gt_nchannel = gt_channel
        self.frame_period = frame_period

        assert exists(self.wav_path), 'wav directory not found!'

        if not exists(self.cmp_path):
            os.makedirs(self.cmp_path)

        if not exists(self.wav_ns_path):
            os.makedirs(self.wav_ns_path)
        self.file_list = self.get_file_list(self.wav_path)

    def extract_feature_file(self, fname):
        # x, fs = librosa.load(join(self.wav_ns_path, fname + '.wav'), sr=16000, mono=True)
        x, fs = librosa.load(join(self.wav_path, fname + '.wav'), sr=16000, mono=True)
        x = x / max(abs(x))
        x = trim_silence(x, threshold=-50, n_win=int(0.1 * fs))
        if len(x) > 12 * fs:
            x = x[0:12 * fs]
        # if len(x) < 0.2 * fs:
        #     return None
        x = x.astype('float64')
        f0, time_axis = pyworld.dio(x, fs, frame_period=5)
        f0 = pyworld.stonemask(x, f0, time_axis, fs)
        f0[f0 < 1e-2] = 1e-2
        f0 = np.log10(f0)
        gfc = self.gfcc(x, fs, numceps=self.gfc_order)
        # gfc = mfcc(x, fs, winstep=0.005, numcep=self.gfc_order)
        rmse = librosa.feature.rmse(y=x, frame_length=128, hop_length=int(fs * 0.005)).T
        gfc = gfc[0:min(gfc.shape[0], f0.shape[0])]
        rmse = rmse[0:len(gfc)]
        f0 = f0[0:len(gfc)]
        f0 = np.expand_dims(f0, axis=-1)

        feat = np.hstack((gfc, rmse, f0))
        delta_feat = delta(feat, 1)
        acc_feat = delta(delta_feat, 1)
        data = np.hstack((feat, delta_feat, acc_feat))
        return data

    def extract_feature_all(self):
        for fname in tqdm(self.file_list):
            data = self.extract_feature_file(fname)
            if data is not None:
                data.astype('float32').tofile(join(self.cmp_path, fname+self.cmp_ext))
            else:
                print 'File %s is removed\n' % fname

    def scale(self, data):
        mean, std = self.mean_var(data)
        data = (data - mean)/std
        return data, mean, std

    def load_all_data(self):
        data_all = []
        for fname in tqdm(self.file_list):
            data = np.fromfile(join(self.cmp_path, fname+self.cmp_ext), dtype=np.float32)
            data = data.reshape(-1, self.gfc_dim)
            data, _, _ = self.scale(data)
            data_all.append(data)
        return data_all

    def noise_reduction(self, fname):
        wav_file = join(self.wav_path, fname+'.wav')
        raw_file = join(self.root_path, 'temp.raw')
        ns_raw_file = join(self.root_path, 'temp_ns.raw')
        ns_wav_48k_file = join(self.root_path, 'temp.wav')
        ns_wav_16k_file = join(self.wav_ns_path, fname+'.wav')

        call(['sox', '-v', '0.98', wav_file, '-r', '48000', '-b', '16', '-e', 'signed-integer', '-c', '1', '-q', raw_file])
        call([rnnoise_bin, raw_file, ns_raw_file])
        call(['sox', '-v', '0.98', '-r', '48000', '-b', '16', '-e', 'signed-integer', '-c', '1', '-q',
              ns_raw_file, ns_wav_48k_file])
        call(['sox', '-v', '0.98', ns_wav_48k_file, '-r', '16000', '-b', '16', '-e', 'signed-integer',
              '-c', '1', '-q', ns_wav_16k_file])

    def run_noise_reduction_all(self):
        for fname in tqdm(self.file_list):
            self.noise_reduction(fname)

    @staticmethod
    def gfcc(x, fs, numceps=26, n_channel=64, frame_period=0.005):
        gtgram = gammatone.gtgram.gtgram(x, fs, 0.02, frame_period, n_channel, 50)
        gtgram = np.cbrt(np.transpose(gtgram))
        gfcc = dct(gtgram, type=2, axis=1, norm='ortho')[:, 0:numceps]
        return gfcc

    @staticmethod
    def get_file_list(wav_path):
        file_list_wav = listdir(wav_path)
        file_list = []
        for fname in file_list_wav:
            name = os.path.splitext(fname)[0]
            ext = os.path.splitext(fname)[-1]
            if ext == '.wav':
                file_list.append(name)
        file_list.sort(key=lambda f: int(filter(str.isdigit, f)))
        return file_list

    @staticmethod
    def mean_var(data):
        mean = data.mean(axis=0)
        std = data.std(axis=0)
        std[std < 1e-5] = 1e-5
        return mean, std


def trim_silence(x, threshold=40, n_win=1600):
    x_e = np.square(x)
    x_e[x_e < 1e-6] = 1e-6
    x_e = 10*np.log10(x_e)
    cumsum = np.convolve(x_e, np.ones((n_win,)) / n_win, mode='same')
    x = x[cumsum > threshold]
    return x


def data_segmentation(x_train, y_train, maxlen=300, frame_shift=150):
    new_segments = []
    new_label = []
    keep_idx = []
    for idx, x in tqdm(enumerate(x_train)):
        x_train[idx] = x
        if len(x) > maxlen:
            s = overlap_segmentation(x, frame_length=maxlen, frame_shift=frame_shift)
            new_segments.extend(s)
            label = np.tile(y_train[idx], (len(s), 1))
            new_label.extend(label)
        elif len(x) > 60:
            keep_idx.append(idx)
    x_train = [x_train[i] for i in keep_idx]
    y_train = [y_train[i] for i in keep_idx]
    x_train.extend(new_segments)
    y_train.extend(new_label)
    return x_train, y_train


def overlap_segmentation(data, frame_length=300, frame_shift=300):
    data_len = len(data)
    if data_len <= frame_length:
        return [data]
    n = 1 + (data_len - frame_length) / frame_shift
    segments = []
    for i in range(n):
        s = data[frame_shift*i: frame_shift*i + frame_length]
        segments.append(s)
    return segments


def do_preprocessing():
    # labels = ['female_north', 'female_central', 'female_south',
    #           'male_north', 'male_central', 'male_south',
    #           'public_test', 'private_test_data']
    root_path = './emodata'
    for l in ['']:
        print 'Extracting features in folder %s' % join(root_path, l)
        corpus = DataProvider(path=join(root_path, l))
        # print 'Performing noise reduction...'
        # corpus.run_noise_reduction_all()
        print 'Extracting features...'
        corpus.extract_feature_all()

    print 'Finished!'


if __name__ == '__main__':
    do_preprocessing()

