import numpy as np
from keras.models import load_model
from tqdm import tqdm
from natsort import natsorted
from os import listdir
import csv
from load_data import load_test_data
n_model = 10


def run_test_accent():
    print 'Load test data...'
    x_test = load_test_data()

    print 'Predicting accent...'
    label_all = []
    
    for i in range(n_model):
        print 'Load accent model %d' % i
        
        label = []
        model = load_model('./checkpoints/saved_models/accent/model_accent.h5')
        model.load_weights('./checkpoints/saved_models/accent/weight_accent_%d.h5' % i)
        for idx, x in tqdm(enumerate(x_test)):
            y = model.predict(x, batch_size=x.shape[0])
            y = y.mean(axis=0)
            label.append(y)
        label_all.append(label)
       
    label_all = np.asarray(label_all, dtype='float32')
    label_all = label_all.mean(axis=0)
    temp = np.array(label_all[:, 0])
    label_all[:, 0] = label_all[:, 1]
    label_all[:, 1] = temp
    label_all = np.argmax(label_all, axis=-1)
    return label_all


def run_test_gender():
    print 'Load test data...'
    x_test = load_test_data()
    print 'Predicting gender...'
    label_all = []

    for i in range(n_model):
        print 'Load gender model %d' % i
        label = []
        model = load_model('./checkpoints/saved_models/gender/model_gender.h5')
        model.load_weights('./checkpoints/saved_models/gender/weights_gender_%d.h5' % i)
        for idx, x in tqdm(enumerate(x_test)):
            y = model.predict(x, batch_size=x.shape[0])
            y = y.mean(axis=0)
            label.append(y)
        label_all.append(label)

    label_all = np.asarray(label_all, dtype='float32')
    label_all = label_all.mean(axis=0)
    label_all = np.argmax(label_all, axis=-1)
    return label_all


def run_test_all():
    result_accent = run_test_accent()
    result_gender = run_test_gender()
    file_list = natsorted(listdir('./data/private_test_data/wav'))
    result_csv = [['id', 'gender', 'accent']]
    for idx, [i, j] in enumerate(zip(result_gender, result_accent)):
        result_csv.append([file_list[idx], i, j])
    f = open('submission.csv', 'w')
    with f:
        writer = csv.writer(f)
        writer.writerows(result_csv)


if __name__ == "__main__":
    run_test_all()
