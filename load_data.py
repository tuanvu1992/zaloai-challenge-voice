import numpy as np
from preprocessing import DataProvider, data_segmentation, overlap_segmentation
from os.path import join, isfile
from tqdm import tqdm

root_path = './data'


def get_public_test_label():
    label = np.zeros((5559, 5))

    label[0:1203, [2, 3]] = 1
    label[1204:1565, [1, 3]] = 1
    label[1566:2240, [0, 4]] = 1
    label[2241:2746, [0, 3]] = 1
    label[2747:3611, [1, 4]] = 1
    label[3612:5558, [2, 4]] = 1

    return label


def get_emo_label(file_list):
    label = np.zeros((len(file_list), 7))
    for idx, fname in enumerate(file_list):
        if fname[5] == 'W':
            label[idx][0] = 1
        if fname[5] == 'L':
            label[idx][1] = 1
        if fname[5] == 'E':
            label[idx][2] = 1
        if fname[5] == 'A':
            label[idx][3] = 1
        if fname[5] == 'F':
            label[idx][4] = 1
        if fname[5] == 'T':
            label[idx][5] = 1
        if fname[5] == 'N':
            label[idx][6] = 1
    return label


def load_emo_data():
    if isfile('./emodata/x_train.npy') and isfile('./emodata/y_train.npy'):
        x_train = np.load('./emodata/x_train.npy')
        y_train = np.load('./emodata/y_train.npy')
        return x_train, y_train
    else:
        x_train = []

        p = DataProvider('./emodata')
        x_train.extend(p.load_all_data())
        y_train = get_emo_label(p.file_list)

        print 'Segmenting data...'
        x_train, y_train = data_segmentation(x_train, y_train, maxlen=600, frame_shift=600)
        print 'Padding data...'
        max_len = len(max(x_train, key=len))
        for idx, x in enumerate(x_train):
            x_train[idx] = np.pad(x, ((0, max_len - len(x)), (0, 0)), 'wrap')

        x_train = np.asarray(x_train, dtype=np.float32)
        y_train = np.asarray(y_train, dtype=np.float32)
        print 'Sample length: %d' % max_len
        print 'Total samples: %d' % len(x_train)
        print 'Saving array at %s' % root_path
        np.save('./emodata/x_train.npy', x_train)
        np.save('./emodata/y_train.npy', y_train)
        return x_train, y_train


def load_train_data():
    labels = ['female_north', 'female_central', 'female_south',
              'male_north', 'male_central', 'male_south']
    label_vectors = np.asarray([[1, 0, 0, 1, 0],
                                [0, 1, 0, 1, 0],
                                [0, 0, 1, 1, 0],
                                [1, 0, 0, 0, 1],
                                [0, 1, 0, 0, 1],
                                [0, 0, 1, 0, 1]], dtype=np.float32)

    if isfile(join(root_path, 'x_train.npy')) and \
            isfile(join(root_path, 'y_train.npy')):
        print 'Loading saved data...'
        x_train = np.load(join(root_path, 'x_train.npy'))
        y_train = np.load(join(root_path, 'y_train.npy'))
        return x_train, y_train
    else:
        x_train = []
        y_train = []
        print 'Reading acoustic feature files...'
        for idx, f in enumerate(labels):
            p = DataProvider(join(root_path, f))
            x_train.extend(p.load_all_data())
            y_train.extend(np.tile(label_vectors[idx], (len(p.file_list), 1)))
        # Load public test data :D
        p = DataProvider(join(root_path, 'public_test'))
        x_train.extend(p.load_all_data())
        y_train.extend(get_public_test_label())

        print 'Segmenting data...'
        x_train, y_train = data_segmentation(x_train, y_train, maxlen=600, frame_shift=600)
        print 'Padding data...'
        max_len = len(max(x_train, key=len))
        for idx, x in enumerate(x_train):
            x_train[idx] = np.pad(x, ((0, max_len - len(x)), (0, 0)), 'wrap')

        x_train = np.asarray(x_train, dtype=np.float32)
        y_train = np.asarray(y_train, dtype=np.float32)
        print 'Sample length: %d' % max_len
        print 'Total samples: %d' % len(x_train)
        print 'Saving array at %s' % root_path
        np.save(join(root_path, 'x_train.npy'), x_train)
        np.save(join(root_path, 'y_train.npy'), y_train)
        return x_train, y_train


def load_test_data():
    print 'Loading test data...'
    test_corpus = DataProvider('/model/features')
    x_test_all = test_corpus.load_all_data()
    x_test = []
    for idx, x in tqdm(enumerate(x_test_all)):
        if x.shape[0] < 600:
            x = np.pad(x, ((0, 600 - x.shape[0]), (0, 0)), 'wrap')
            x = np.expand_dims(x, axis=0)
        elif x.shape[0] > 600:
            x = overlap_segmentation(x, frame_length=600, frame_shift=300)
            x[-1] = np.pad(x[-1], ((0, 600 - x[-1].shape[0]), (0, 0)), 'wrap')
            x = np.asarray(x)
        else:
            x = np.expand_dims(x, 0)
        x_test.append(x)
    return x_test

